package main

import (
	"context"

	"gitlab.com/thebikepark/bikepark-geo-search/internal/app"
	"gitlab.com/thebikepark/bikepark-geo-search/internal/geo"
	bpRedis "gitlab.com/thebikepark/bikepark-geo-search/internal/redis"
	"gitlab.com/thebikepark/bikepark-geo-search/internal/rpc"
	"gitlab.com/thebikepark/bikepark-geo-search/internal/server"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

func main() {
	c, err := app.ProcessConfig()
	if err != nil {
		panic(err)
	}

	var logger *zap.Logger
	if c.Environment == "production" {
		w := zapcore.AddSync(&lumberjack.Logger{
			Filename:   c.LogOutput,
			MaxSize:    500, // megabytes
			MaxBackups: 3,
			MaxAge:     28, // days
		})
		core := zapcore.NewCore(
			zapcore.NewJSONEncoder(zap.NewProductionEncoderConfig()),
			w,
			zap.DebugLevel,
		)
		logger = zap.New(core)
	} else {
		var err error
		logger, err = zap.NewDevelopment()
		if err != nil {
			panic(err)
		}
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	locator := geo.NewOpenStreetMap(c.GeoServiceURL, "jsonv2")
	redisManager := bpRedis.NewRedisManager(bpRedis.Config{
		Address:  c.RedisAddress,
		Password: c.RedisPassword,
		Timeout:  c.RedisTimeout,
	})
	if err := redisManager.Ping(ctx); err != nil {
		logger.Warn("redis is not connected. Please check redis connection to use cache", zap.Error(err))
	}
	defer redisManager.Close()

	logger.Info("starting bikepark geo search gprc server on port " + c.Port)

	s := server.New(locator, redisManager, logger)
	err = rpc.StartServer(ctx, s, c.Port)
	if err != nil {
		logger.Error("shuting down bikepark geo search server", zap.Error(err))
	}
}
