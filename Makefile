MAIN_PATH=cmd/bikepark-geo-search/main.go
VERSION:=1.0.0
PROTOSRC=./api
PROTO_PATH=$(PROTOSRC)/bikepark-geo-search/*.proto
GOARCH:=amd64
GOOS:=linux
DOCKERNET:=bikepark
PRODUCT:=bikepark-geo-search
BUILDFLAGS:=""
BUILD:=$(shell date +%s)
GOENVS:=CGO_ENABLED=0


export GO111MODULE=on
export GOPRIVATE=gitlab.com/thebikepark/bikepark-geo-search

################################################################################
# LINT
################################################################################

.PHONY: lint-deps
lint-deps: ## get linter for testing
	GO111MODULE=on go get github.com/golangci/golangci-lint/cmd/golangci-lint

.PHONY:
lint-ci: ## get linter for testing
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.43.0
	$(go env GOPATH)/bin/golangci-lint run --skip-dirs api/

.PHONY: lint
lint:
	golangci-lint run --skip-dirs api/

################################################################################
# BUILD
################################################################################

.PHONY: build-deps
build-deps:
	go mod tidy

.PHONY: build
build: build-deps ## build the base bikepark application
	GOOS=$(GOOS) GOARCH=$(GOARCH) $(GOENVS) go build -v -ldflags \
		"-X main.Version=$(VERSION) -X main.Build=$(BUILD) $(LDFLAGS)" \
		-tags $(TAGS) \
		$(BUILDFLAGS) -o $(PRODUCT) $(MAIN_PATH)

.PHONY:vendor
vendor:
	go mod vendor

################################################################################
# DOCKER
################################################################################
.PHONY: docker-network
docker-network: ## spin up the local gym docker network so that all dockerized gym components can communicate
	if [ -z $$(docker network ls -q --filter 'name=$(DOCKERNET)') ]; then\
		docker network create $(DOCKERNET);\
	fi

.PHONY: docker-up
docker-up: vendor docker-network
	docker-compose --project-directory . \
	-f deployments/docker-compose/docker-compose.yml \
	up \
	-d \
	--force-recreate \
	--build \
	--remove-orphans

.PHONY: docker-logs
docker-logs:
	docker-compose  --project-directory . \
	-f deployments/docker-compose/docker-compose.yml \
	logs

.PHONY: docker-config
docker-config:
	docker-compose --project-directory . \
	-f deployments/docker-compose/docker-compose.yml \
	config

################################################################################
# TEST
################################################################################
.PHONY: test
test: ## run unit tests and code coverage
	go vet ./...
	go test ./... -v
	go test ./... -cover

################################################################################
# PROTO
# ################################################################################

.PHONY:proto
proto:
	protoc -I$(PROTOSRC)/include \
		-I$(PROTOSRC)/bikepark-geo-search \
		--go_out=./pkg --go_opt=paths=source_relative\
		--go-grpc_out=./pkg --go-grpc_opt=paths=source_relative\
		--grpc-gateway_out=logtostderr=true,paths=source_relative:./pkg \
		--openapiv2_out=logtostderr=true:docs \
		$(PROTO_PATH)
