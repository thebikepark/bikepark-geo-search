package server

import (
	"context"
	"crypto/md5"
	"errors"
	"fmt"

	bikepark_geo_v1 "gitlab.com/thebikepark/bikepark-geo-search/pkg"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	searchErr                   = "There was an error"
	geoRedisKeyBlueprint        = "bp:geo-loc:%x"
	reverseGeoRedisKeyBlueprint = "bp:geo-reverse-loc:%f:%f"
)

// SearchGeoLocation search for location from third party API by address
func (s *Service) SearchGeoLocation(ctx context.Context, in *bikepark_geo_v1.SearchGeoLocationRequest) (*bikepark_geo_v1.SearchGeoLocationResponse, error) {
	log := s.log.With(
		zap.String("handler", "internal.server.Service.SearchGeoLocation"),
	)

	if in.Filter == nil {
		log.Error("missing filter", zap.Error(errors.New("no filter")))
		return nil, status.New(codes.InvalidArgument, "Filter is required").Err()
	}
	if in.Filter.Address == "" {
		invalidAddrs := Details("address", "Address is required")
		err := NewValidationErr(errors.New("invalid address"), invalidAddrs)
		log.Error("missing address", zap.Error(err))
		return nil, NewAPIStatusErr(err, "Invalid Arguments").Err()
	}

	sum := md5.Sum([]byte(in.Filter.GetAddress()))
	key := fmt.Sprintf(geoRedisKeyBlueprint, sum)
	geoLocs, err := s.cache.List(ctx, key)
	if err != nil {
		log.Error("failed to get locations from cache", zap.Error(err))
	}

	if len(geoLocs) > 0 {
		log.Info("returning geo locations from cache")
		return &bikepark_geo_v1.SearchGeoLocationResponse{
			Locations: geoLocs,
		}, nil
	}

	locations, err := s.locator.Search(in.Filter.GetAddress())
	if err != nil {
		log.Error("failed to get locations", zap.Error(err))
		return nil, status.New(codes.Internal, searchErr).Err()
	}

	geoLocs, err = locations.ToProtos()
	if err != nil {
		log.Error("failed to parse locations", zap.Error(err))
		return nil, status.New(codes.Internal, searchErr).Err()
	}

	if err := s.cache.Store(ctx, key, geoLocs); err != nil {
		log.Error("failed to store locations in cache", zap.Error(err))
	}

	log.Debug("request successfully finished")

	return &bikepark_geo_v1.SearchGeoLocationResponse{
		Locations: geoLocs,
	}, nil
}

// ReverseGeoLocation search for location from third party API by latitude, longitude
func (s *Service) ReverseGeoLocation(ctx context.Context, in *bikepark_geo_v1.ReverseGeoLocationRequest) (*bikepark_geo_v1.GeoLocation, error) {
	log := s.log.With(
		zap.String("handler", "internal.server.Service.ReverseGeoLocation"),
	)

	if in.Filter == nil {
		log.Error("missing filter", zap.Error(errors.New("no filter")))
		return nil, status.New(codes.InvalidArgument, "Filter is required").Err()
	}
	if in.Filter.GetLatitude() > 90 || in.Filter.GetLatitude() < -90 {
		invalidLat := Details("latitude", "Invalid latitude range. Must be between 90 and -90")
		err := NewValidationErr(errors.New("invalid latitude"), invalidLat)
		log.Error("invalid latitude", zap.Error(err))
		return nil, NewAPIStatusErr(err, "Invalid Arguments").Err()
	}
	if in.Filter.GetLongitude() > 180 || in.Filter.GetLongitude() < -180 {
		invalidLng := Details("longitude", "Invalid longitude range. Must be between 180 and -180")
		err := NewValidationErr(errors.New("invalid longitude"), invalidLng)
		log.Error("invalid longitude", zap.Error(err))
		return nil, NewAPIStatusErr(err, "Invalid Arguments").Err()
	}

	key := fmt.Sprintf(reverseGeoRedisKeyBlueprint, in.Filter.GetLatitude(), in.Filter.GetLongitude())
	loc, err := s.cache.Get(ctx, key)
	if err != nil {
		log.Error("failed to get locations from cache", zap.Error(err))
	}
	if loc != nil {
		log.Info("returning location from cache")
		return loc, nil
	}

	location, err := s.locator.Reverse(in.Filter.GetLatitude(), in.Filter.GetLongitude())
	if err != nil {
		log.Error("failed to get locations", zap.Error(err))
		return nil, status.New(codes.Internal, searchErr).Err()
	}
	geoLoc, err := location.ToGeoLocation()
	if err != nil {
		log.Error("failed to parse locations", zap.Error(err))
		return nil, status.New(codes.Internal, searchErr).Err()
	}

	if err := s.cache.Store(ctx, key, geoLoc); err != nil {
		log.Error("failed to store locations in cache", zap.Error(err))
	}

	log.Debug("request successfully finished")

	return geoLoc, nil
}
