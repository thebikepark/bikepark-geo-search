package server

import (
	"context"

	"gitlab.com/thebikepark/bikepark-geo-search/internal/geo"
	bikepark_geo_v1 "gitlab.com/thebikepark/bikepark-geo-search/pkg"
	"go.uber.org/zap"
)

type LocationsCacher interface {
	Store(ctx context.Context, key string, v interface{}) error
	Get(ctx context.Context, key string) (*bikepark_geo_v1.GeoLocation, error)
	List(ctx context.Context, key string) ([]*bikepark_geo_v1.GeoLocation, error)
}

var _ bikepark_geo_v1.BikeParkGeoSearchServer = &Service{}

// Service implements bikepark geo search server
type Service struct {
	locator geo.Locator
	cache   LocationsCacher
	log     *zap.Logger
	*bikepark_geo_v1.UnimplementedBikeParkGeoSearchServer
}

// New returns new instance of bikepark geo search service
func New(l geo.Locator, lc LocationsCacher, log *zap.Logger) *Service {
	return &Service{
		locator: l,
		cache:   lc,
		log:     log,
	}
}
