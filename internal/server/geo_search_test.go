package server

import (
	"context"
	"errors"
	"testing"

	"github.com/c2fo/testify/require"
	"gitlab.com/thebikepark/bikepark-geo-search/internal/geo"
	bikepark_geo_v1 "gitlab.com/thebikepark/bikepark-geo-search/pkg"
	"go.uber.org/zap"
)

func TestServiceSearchGeoLocation(t *testing.T) {
	type fields struct {
		locator geo.Locator
		cache   LocationsCacher
	}
	type args struct {
		ctx context.Context
		in  *bikepark_geo_v1.SearchGeoLocationRequest
	}
	tests := []struct {
		name      string
		fields    fields
		args      args
		want      *bikepark_geo_v1.SearchGeoLocationResponse
		wantErr   bool
		errString string
	}{
		{
			name: "it should return response with locations",
			fields: fields{
				locator: &mockLocator{
					search: func(address string) (geo.Locations, error) {
						return locationsMock(), nil
					},
				},
				cache: &mockRedisManager{},
			},
			args: args{
				ctx: context.Background(),
				in: &bikepark_geo_v1.SearchGeoLocationRequest{
					Filter: &bikepark_geo_v1.AddressSearchFilter{
						Address: "test",
					},
				},
			},
			want: &bikepark_geo_v1.SearchGeoLocationResponse{
				Locations: []*bikepark_geo_v1.GeoLocation{
					{
						Address:     "Test street",
						City:        "Zagreb",
						DisplayName: "Test street, 1111, Croatia, HR",
						Latitude:    1,
						Longitude:   2,
					},
					{
						Address:     "Test street 2",
						City:        "Split",
						DisplayName: "Test street, 2222, Croatia, HR",
						Latitude:    1,
						Longitude:   2,
					},
				},
			},
		},
		{
			name: "it should return an error",
			fields: fields{
				locator: &mockLocator{
					search: func(address string) (geo.Locations, error) {
						return nil, errors.New("open street map error")
					},
				},
				cache: &mockRedisManager{},
			},
			args: args{
				ctx: context.Background(),
				in: &bikepark_geo_v1.SearchGeoLocationRequest{
					Filter: &bikepark_geo_v1.AddressSearchFilter{
						Address: "test",
					},
				},
			},
			wantErr:   true,
			errString: "rpc error: code = Internal desc = There was an error",
		},
		{
			name: "it should return no filter error",
			fields: fields{
				locator: &mockLocator{},
				cache:   &mockRedisManager{},
			},
			args: args{
				ctx: context.Background(),
				in:  &bikepark_geo_v1.SearchGeoLocationRequest{},
			},
			wantErr:   true,
			errString: "rpc error: code = InvalidArgument desc = Filter is required",
		},
		{
			name: "it should return no address in filter",
			fields: fields{
				locator: &mockLocator{},
				cache:   &mockRedisManager{},
			},
			args: args{
				ctx: context.Background(),
				in: &bikepark_geo_v1.SearchGeoLocationRequest{
					Filter: &bikepark_geo_v1.AddressSearchFilter{
						Address: "",
					},
				},
			},
			wantErr:   true,
			errString: "rpc error: code = InvalidArgument desc = Invalid Arguments",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Service{
				locator: tt.fields.locator,
				cache:   tt.fields.cache,
				log:     zap.NewNop(),
			}
			got, err := s.SearchGeoLocation(tt.args.ctx, tt.args.in)
			if tt.wantErr {
				require.Error(t, err)
				require.EqualError(t, err, tt.errString)
				return
			}
			require.NoError(t, err)
			require.Equal(t, tt.want, got)
		})
	}
}

func TestServiceReverseGeoLocation(t *testing.T) {
	type fields struct {
		locator geo.Locator
		cache   LocationsCacher
	}
	type args struct {
		ctx context.Context
		in  *bikepark_geo_v1.ReverseGeoLocationRequest
	}
	tests := []struct {
		name      string
		fields    fields
		args      args
		want      *bikepark_geo_v1.GeoLocation
		wantErr   bool
		errString string
	}{
		{
			name: "it should return locations",
			args: args{
				ctx: context.Background(),
				in: &bikepark_geo_v1.ReverseGeoLocationRequest{
					Filter: &bikepark_geo_v1.ReverseSearchFilter{
						Latitude:  85,
						Longitude: 100,
					},
				},
			},
			want: &bikepark_geo_v1.GeoLocation{
				Address:     "Test street",
				City:        "Zagreb",
				DisplayName: "Test street, 1111, Croatia, HR",
				Latitude:    1,
				Longitude:   2,
			},
			fields: fields{
				locator: &mockLocator{
					reverse: func(lat, long float64) (geo.Location, error) {
						return locationsMock()[0], nil
					},
				},
				cache: &mockRedisManager{},
			},
		},
		{
			name: "it should return locatior search error",
			args: args{
				ctx: context.Background(),
				in: &bikepark_geo_v1.ReverseGeoLocationRequest{
					Filter: &bikepark_geo_v1.ReverseSearchFilter{
						Latitude:  85,
						Longitude: 100,
					},
				},
			},
			fields: fields{
				locator: &mockLocator{
					reverse: func(lat, long float64) (geo.Location, error) {
						return nil, errors.New("reverse search error")
					},
				},
				cache: &mockRedisManager{},
			},
			wantErr:   true,
			errString: "rpc error: code = Internal desc = There was an error",
		},
		{
			name: "it should return no filter error",
			args: args{
				ctx: context.Background(),
				in:  &bikepark_geo_v1.ReverseGeoLocationRequest{},
			},
			fields: fields{
				locator: &mockLocator{
					reverse: func(lat, long float64) (geo.Location, error) {
						return locationsMock()[0], nil
					},
				},
				cache: &mockRedisManager{},
			},
			wantErr:   true,
			errString: "rpc error: code = InvalidArgument desc = Filter is required",
		},
		{
			name: "it should return invalid latitude ( > 90)",
			args: args{
				ctx: context.Background(),
				in: &bikepark_geo_v1.ReverseGeoLocationRequest{
					Filter: &bikepark_geo_v1.ReverseSearchFilter{
						Latitude:  111,
						Longitude: 1,
					},
				},
			},
			fields: fields{
				locator: &mockLocator{
					reverse: func(lat, long float64) (geo.Location, error) {
						return locationsMock()[0], nil
					},
				},
				cache: &mockRedisManager{},
			},
			wantErr:   true,
			errString: "rpc error: code = InvalidArgument desc = Invalid Arguments",
		},
		{
			name: "it should return invalid latitude ( < -90)",
			args: args{
				ctx: context.Background(),
				in: &bikepark_geo_v1.ReverseGeoLocationRequest{
					Filter: &bikepark_geo_v1.ReverseSearchFilter{
						Latitude:  -91,
						Longitude: 1,
					},
				},
			},
			fields: fields{
				locator: &mockLocator{
					reverse: func(lat, long float64) (geo.Location, error) {
						return locationsMock()[0], nil
					},
				},
				cache: &mockRedisManager{},
			},
			wantErr:   true,
			errString: "rpc error: code = InvalidArgument desc = Invalid Arguments",
		},
		{
			name: "it should return invalid longitude ( > 180)",
			args: args{
				ctx: context.Background(),
				in: &bikepark_geo_v1.ReverseGeoLocationRequest{
					Filter: &bikepark_geo_v1.ReverseSearchFilter{
						Latitude:  1,
						Longitude: 181,
					},
				},
			},
			fields: fields{
				locator: &mockLocator{
					reverse: func(lat, long float64) (geo.Location, error) {
						return locationsMock()[0], nil
					},
				},
				cache: &mockRedisManager{},
			},
			wantErr:   true,
			errString: "rpc error: code = InvalidArgument desc = Invalid Arguments",
		},
		{
			name: "it should return invalid longitude ( < -180)",
			args: args{
				ctx: context.Background(),
				in: &bikepark_geo_v1.ReverseGeoLocationRequest{
					Filter: &bikepark_geo_v1.ReverseSearchFilter{
						Latitude:  1,
						Longitude: -181,
					},
				},
			},
			fields: fields{
				locator: &mockLocator{
					reverse: func(lat, long float64) (geo.Location, error) {
						return locationsMock()[0], nil
					},
				},
				cache: &mockRedisManager{},
			},
			wantErr:   true,
			errString: "rpc error: code = InvalidArgument desc = Invalid Arguments",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Service{
				locator: tt.fields.locator,
				cache:   tt.fields.cache,
				log:     zap.NewNop(),
			}
			got, err := s.ReverseGeoLocation(tt.args.ctx, tt.args.in)
			if tt.wantErr {
				require.Error(t, err)
				require.EqualError(t, err, tt.errString)
				return
			}
			require.NoError(t, err)
			require.Equal(t, tt.want, got)
		})
	}
}

type mockLocator struct {
	search  func(string) (geo.Locations, error)
	reverse func(float64, float64) (geo.Location, error)
}

func (ml *mockLocator) Search(address string) (geo.Locations, error) {
	return ml.search(address)
}

func (ml *mockLocator) Reverse(lat, lng float64) (geo.Location, error) {
	return ml.reverse(lat, lng)
}

func locationsMock() geo.Locations {
	l1 := &geo.OpenStreetMapLocation{
		DisplayName: "Test street, 1111, Croatia, HR",
		Address: geo.OpenStreetMapAddress{
			City:        "Zagreb",
			Country:     "Croatia",
			CountryCode: "HR",
			Postcode:    "1",
			Road:        "Test street",
			Suburb:      "Test suburb",
		},
		BoundingBox: []string{"1", "2", "3", "4"},
		Latitude:    "1",
		Longitude:   "2",
		Licence:     "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
		Class:       "highway",
		OsmID:       1,
		Importance:  0.5,
		OsmType:     "way",
		PlaceID:     1,
		Type:        "residential",
	}
	l2 := &geo.OpenStreetMapLocation{
		DisplayName: "Test street, 2222, Croatia, HR",
		Address: geo.OpenStreetMapAddress{
			City:        "Split",
			Country:     "Croatia",
			CountryCode: "HR",
			Postcode:    "2",
			Road:        "Test street 2",
			Suburb:      "Test suburb",
		},
		BoundingBox: []string{"1", "2", "3", "4"},
		Latitude:    "1",
		Longitude:   "2",
		Licence:     "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
		Class:       "highway",
		OsmID:       2,
		Importance:  0.5,
		OsmType:     "way",
		PlaceID:     2,
		Type:        "residential",
	}
	return geo.Locations{l1, l2}
}

type mockRedisManager struct {
}

func (rm *mockRedisManager) Store(ctx context.Context, key string, v interface{}) error {
	return nil
}

func (rm *mockRedisManager) List(ctx context.Context, key string) ([]*bikepark_geo_v1.GeoLocation, error) {
	return nil, nil
}

func (rm *mockRedisManager) Get(ctx context.Context, key string) (*bikepark_geo_v1.GeoLocation, error) {
	return nil, nil
}
