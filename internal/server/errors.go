package server

import (
	"errors"

	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"
)

const (
	errInternal = "An error occurred"
)

// NewAPIStatusErr returs new status error with details
func NewAPIStatusErr(e error, msg string, d ...proto.Message) *status.Status {
	var validationErr *ValidationErr
	if errors.As(e, &validationErr) {
		s, err := status.New(codes.InvalidArgument, msg).WithDetails(validationErr.BadRequestDetails)
		if err != nil {
			return status.New(codes.Internal, errInternal)
		}
		return s
	}
	return status.New(codes.Internal, errInternal)
}

// InternalServerError returns internal server error
func InternalServerError() *status.Status {
	return NewAPIStatusErr(nil, "")
}

// ValidationErr holds info for not found error
type ValidationErr struct {
	Err               error
	BadRequestDetails *errdetails.BadRequest
}

// NewValidationErr returns new validation error
func NewValidationErr(err error, details *errdetails.BadRequest) *ValidationErr {
	return &ValidationErr{err, details}
}

func (ve *ValidationErr) Error() string {
	return ve.Err.Error()
}

// Details returns proto message of validation details
func Details(field, description string) *errdetails.BadRequest {
	return &errdetails.BadRequest{
		FieldViolations: []*errdetails.BadRequest_FieldViolation{
			{
				Field:       field,
				Description: description,
			},
		},
	}
}

// Is match error in error chain
func (ve *ValidationErr) Is(err error) bool {
	_, ok := err.(*ValidationErr)
	return ok
}
