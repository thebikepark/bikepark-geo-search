package auth

import (
	"context"

	"gitlab.com/thebikepark/bikepark-geo-search/internal/app"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	protoService = "bikepark.geo.v1.BikeParkGeoSearch"
	all          = "ALL"
	// ADMIN role
	ADMIN = "ADMIN"
	// USER role
	USER = "USER"
)

var (
	endpointAccess = map[string][]string{
		"/" + protoService + "/SearchGeoLocation":  {all},
		"/" + protoService + "/ReverseGeoLocation": {all},
	}
	publicMethods = map[string][]string{
		"/" + protoService + "/SearchGeoLocation":  {all},
		"/" + protoService + "/ReverseGeoLocation": {all},
	}
)

// AuthorizationInterceptor validate authorization token
func AuthorizationInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
	method := info.FullMethod
	if isPublic(method) {
		return handler(ctx, req)
	}
	roles, ok := endpointAccess[method]
	if !ok {
		return nil, status.New(codes.NotFound, "Endpoint does not exist").Err()
	}
	r, err := app.RequesterFromCtx(ctx)
	if err != nil {
		return nil, status.New(codes.InvalidArgument, "Bad Token").Err()
	}

	for _, role := range roles {
		if role == all || role == r.Role {
			return handler(ctx, req)
		}
	}
	return nil, status.New(codes.PermissionDenied, "Access denied").Err()
}

// isPublic checks is method public
func isPublic(m string) bool {
	_, ok := publicMethods[m]
	return ok
}
