package rpc

import (
	"context"
	"log"
	"net"
	"os"
	"os/signal"

	"github.com/pkg/errors"
	"gitlab.com/thebikepark/bikepark-geo-search/internal/auth"
	geo "gitlab.com/thebikepark/bikepark-geo-search/pkg"
	"google.golang.org/grpc"
)

// StartServer will start grpc server for user management
func StartServer(ctx context.Context, s geo.BikeParkGeoSearchServer, port string) error {
	sock, err := net.Listen("tcp", preparePort(port))
	if err != nil {
		return errors.Wrap(err, "failed to bind sock listener")
	}

	opts := []grpc.ServerOption{grpc.UnaryInterceptor(auth.AuthorizationInterceptor)}
	grpcServer := grpc.NewServer(opts...)
	geo.RegisterBikeParkGeoSearchServer(grpcServer, s)

	// graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c

		// sig is a ^C, handle it
		log.Println("shutting down gRPC server...")
		// TODO: fix this
		grpcServer.GracefulStop()

		<-ctx.Done()
	}()

	return grpcServer.Serve(sock)
}

func preparePort(port string) string {
	if string(port[0]) != ":" {
		return ":" + port
	}

	return port
}
