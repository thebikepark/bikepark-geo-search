package app

// BaseErr is base bike error
type BaseErr struct {
	err error
	msg string
}

// NewBaseError returns new base error
func NewBaseError(err error, msg string) *BaseErr {
	return &BaseErr{err, msg}
}

func (be *BaseErr) Error() string {
	return be.err.Error()
}
