package app

import (
	"fmt"
	"time"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Service       string        `envconfig:"SERVICE" default:"bikepark-geo-search"`
	Version       string        `envconfig:"VERSION" default:"0.0.1"`
	Environment   string        `envconfig:"ENVIRONMENT" default:"production"`
	Port          string        `envconfig:"PORT" default:":50002"`
	GeoServiceURL string        `envconfig:"GEO_SERVICE_URL" default:"https://nominatim.openstreetmap.org"`
	LogOutput     string        `envconfig:"LOG_OUTPUT_PATH" default:"/var/log/messages"`
	LogLevel      string        `envconfig:"LOG_LEVEL" default:"DEBUG"`
	RedisAddress  string        `envconfig:"REDIS_ADDRESS" default:"localhost:6379"`
	RedisPassword string        `envconfig:"REDIS_PASSWORD"`
	RedisTimeout  time.Duration `envconfig:"REDIS_TIMEOUT" default:"3s"`
}

func ProcessConfig() (Config, error) {
	var c Config
	if err := envconfig.Process("", &c); err != nil {
		return c, fmt.Errorf("parse env configuration: %w", err)
	}
	return c, nil
}
