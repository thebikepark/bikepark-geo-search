package app

import (
	"context"
	"errors"

	"gitlab.com/rasparac/bikepark/pkg/auth"
	"google.golang.org/grpc/metadata"
)

// Requester hold request data extracted from incoming context
type Requester struct {
	Username string
	Role     string
	Name     string
}

// RequesterFromCtx returns username and role from incoming context
func RequesterFromCtx(ctx context.Context) (Requester, error) {
	r := Requester{}
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return r, errors.New("failed to get metadata from incoming context")
	}
	username := md.Get(auth.UsernameHeaderKey)
	if len(username) < 1 {
		return r, errors.New("no username in context metadata")
	}
	role := md.Get(auth.RoleHeaderKey)
	if len(role) < 1 {
		return r, errors.New("no role in context metadata")
	}
	name := md.Get(auth.NameHeaderKey)
	if len(name) < 1 {
		return r, errors.New("no name in context medata")
	}
	r.Role = role[0]
	r.Username = username[0]
	r.Name = name[0]
	return r, nil
}
