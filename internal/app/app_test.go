package app

import (
	"context"
	"testing"

	"github.com/c2fo/testify/require"
	"gitlab.com/rasparac/bikepark/pkg/auth"
	"google.golang.org/grpc/metadata"
)

func TestRequesterFromCtx(t *testing.T) {
	ctx := context.Background()
	fullMetadata := metadata.NewIncomingContext(ctx, metadata.MD{
		auth.RoleHeaderKey:     []string{"test-role"},
		auth.UsernameHeaderKey: []string{"test"},
		auth.NameHeaderKey:     []string{"name/1"},
	})
	noRole := metadata.NewIncomingContext(ctx, metadata.MD{
		auth.RoleHeaderKey:     []string{},
		auth.UsernameHeaderKey: []string{"test"},
		auth.NameHeaderKey:     []string{"name/1"},
	})
	noUsername := metadata.NewIncomingContext(ctx, metadata.MD{
		auth.RoleHeaderKey:     []string{"test-role"},
		auth.UsernameHeaderKey: []string{},
		auth.NameHeaderKey:     []string{"name/1"},
	})
	noName := metadata.NewIncomingContext(ctx, metadata.MD{
		auth.RoleHeaderKey:     []string{"test-role"},
		auth.UsernameHeaderKey: []string{"test"},
		auth.NameHeaderKey:     []string{},
	})
	outgoing := metadata.NewOutgoingContext(ctx, nil)

	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		args    args
		want    Requester
		wantErr bool
	}{
		{
			name: "should return username role",
			args: args{
				ctx: fullMetadata,
			},
			want: Requester{
				Name:     "name/1",
				Role:     "test-role",
				Username: "test",
			},
		},
		{
			name: "should return no role in context",
			args: args{
				ctx: noRole,
			},
			wantErr: true,
		},
		{
			name: "should return no username in context",
			args: args{
				ctx: noUsername,
			},
			wantErr: true,
		},
		{
			name: "should return no name in context",
			args: args{
				ctx: noName,
			},
			wantErr: true,
		},
		{
			name: "should return if outgoing context is passed",
			args: args{
				ctx: outgoing,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r, err := RequesterFromCtx(tt.args.ctx)
			if tt.wantErr {
				require.Error(t, err)
				return
			}
			require.NoError(t, err)
			require.Equal(t, tt.want, r)
		})
	}
}
