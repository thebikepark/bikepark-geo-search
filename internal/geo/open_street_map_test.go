package geo

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/c2fo/testify/require"
)

func TestOpenStreetMapSearch(t *testing.T) {
	type fields struct {
		URL        string
		Format     string
		HTTPClient Client
	}
	type args struct {
		q string
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		clientResFn MockDo
		want        Locations
		wantErr     bool
		errString   string
	}{
		{
			name: "it should return open street map locations",
			args: args{
				q: "golang street",
			},
			fields: fields{
				Format: "json",
				URL:    "https://test.com",
			},
			clientResFn: func(_ *http.Request) (*http.Response, error) {
				a := &http.Response{
					Status:     "200 OK",
					StatusCode: 200,
				}
				var buff bytes.Buffer
				_, err := buff.Write(locationsAsBytes())
				if err != nil {
					buff.Write([]byte(""))
				}
				a.Body = ioutil.NopCloser(&buff)
				return a, nil
			},
			want: Locations{
				&OpenStreetMapLocation{
					DisplayName: "Test street, 1111, Croatia, HR",
					Address: OpenStreetMapAddress{
						City:        "Zagreb",
						Country:     "Croatia",
						CountryCode: "HR",
						Postcode:    "1",
						Road:        "Test street",
						Suburb:      "Test suburb",
					},
					BoundingBox: []string{"1", "2", "3", "4"},
					Latitude:    "1",
					Longitude:   "2",
					Licence:     "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
					Class:       "highway",
					OsmID:       1,
					Importance:  0.5,
					OsmType:     "way",
					PlaceID:     1,
					Type:        "residential",
				},
				&OpenStreetMapLocation{
					DisplayName: "Test street, 2222, Croatia, HR",
					Address: OpenStreetMapAddress{
						City:        "Split",
						Country:     "Croatia",
						CountryCode: "HR",
						Postcode:    "2",
						Road:        "Test street",
						Suburb:      "Test suburb",
					},
					BoundingBox: []string{"1", "2", "3", "4"},
					Latitude:    "1",
					Longitude:   "2",
					Licence:     "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
					Class:       "highway",
					OsmID:       2,
					Importance:  0.5,
					OsmType:     "way",
					PlaceID:     2,
					Type:        "residential",
				},
			},
		},
		{
			name: "it should return bad request error",
			args: args{
				q: "golang street",
			},
			fields: fields{
				Format: "json",
				URL:    "https://test.com",
			},
			clientResFn: func(_ *http.Request) (*http.Response, error) {
				a := &http.Response{
					Status:     "400 OK",
					StatusCode: 400,
				}
				a.Body = ioutil.NopCloser(bytes.NewBuffer([]byte("")))
				return a, nil
			},
			wantErr:   true,
			errString: "fetch data from OpenStreetMap service, code: 400",
		},
		{
			name: "it should return decode error",
			args: args{
				q: "golang street",
			},
			fields: fields{
				Format: "json",
				URL:    "https://test.com",
			},
			clientResFn: func(_ *http.Request) (*http.Response, error) {
				a := &http.Response{
					Status:     "200 OK",
					StatusCode: 200,
				}
				var buff bytes.Buffer
				_, err := buff.Write([]byte("aaaa"))
				if err != nil {
					buff.Write([]byte(""))
				}
				a.Body = ioutil.NopCloser(&buff)
				return a, nil
			},
			wantErr:   true,
			errString: "d.Decode(&osmLs) to proper to OpenStreetMap data: invalid character 'a' looking for beginning of value",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			osm := &OpenStreetMap{
				URL:    tt.fields.URL,
				Format: tt.fields.Format,
			}
			client := newMockClient(tt.clientResFn)
			osm.HTTPClient = client
			got, err := osm.Search(tt.args.q)
			if tt.wantErr {
				require.Error(t, err)
				require.EqualError(t, err, tt.errString)
				return
			}
			require.NoError(t, err)
			require.Equal(t, tt.want, got)
		})
	}
}

func TestOpenStreetMapReverse(t *testing.T) {
	type fields struct {
		URL        string
		Format     string
		HTTPClient Client
	}
	type args struct {
		lat float64
		lng float64
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		clientResFn MockDo
		want        Location
		wantErr     bool
		errString   string
	}{
		{
			name: "it should return open street map locations",
			args: args{
				lat: 10,
				lng: 10,
			},
			fields: fields{
				Format: "json",
				URL:    "https://test.com",
			},
			clientResFn: func(_ *http.Request) (*http.Response, error) {
				a := &http.Response{
					Status:     "200 OK",
					StatusCode: 200,
				}
				var buff bytes.Buffer
				_, err := buff.Write(locationAsBytes())
				if err != nil {
					buff.Write([]byte(""))
				}
				a.Body = ioutil.NopCloser(&buff)
				return a, nil
			},
			want: &OpenStreetMapLocation{
				DisplayName: "Test street, 1111, Croatia, HR",
				Address: OpenStreetMapAddress{
					City:        "Zagreb",
					Country:     "Croatia",
					CountryCode: "HR",
					Postcode:    "1",
					Road:        "Test street",
					Suburb:      "Test suburb",
				},
				BoundingBox: []string{"1", "2", "3", "4"},
				Latitude:    "1",
				Longitude:   "2",
				Licence:     "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
				Class:       "highway",
				OsmID:       1,
				Importance:  0.5,
				OsmType:     "way",
				PlaceID:     1,
				Type:        "residential",
			},
		},
		{
			name: "it should return bad request error",
			args: args{
				lat: 10,
				lng: 10,
			},
			fields: fields{
				Format: "json",
				URL:    "https://test.com",
			},
			clientResFn: func(_ *http.Request) (*http.Response, error) {
				a := &http.Response{
					Status:     "400 OK",
					StatusCode: 400,
				}
				a.Body = ioutil.NopCloser(bytes.NewBuffer([]byte("")))
				return a, nil
			},
			wantErr:   true,
			errString: "fetch data from OpenStreetMap service, code: 400",
		},
		{
			name: "it should return decode error",
			args: args{
				lat: 10,
				lng: 10,
			},
			fields: fields{
				Format: "json",
				URL:    "https://test.com",
			},
			clientResFn: func(_ *http.Request) (*http.Response, error) {
				a := &http.Response{
					Status:     "200 OK",
					StatusCode: 200,
				}
				var buff bytes.Buffer
				_, err := buff.Write([]byte("aaaa"))
				if err != nil {
					buff.Write([]byte(""))
				}
				a.Body = ioutil.NopCloser(&buff)
				return a, nil
			},
			wantErr:   true,
			errString: "d.Decode(&osmLs) to proper to OpenStreetMap data: invalid character 'a' looking for beginning of value",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			osm := &OpenStreetMap{
				URL:    tt.fields.URL,
				Format: tt.fields.Format,
			}
			client := newMockClient(tt.clientResFn)
			osm.HTTPClient = client
			got, err := osm.Reverse(tt.args.lat, tt.args.lng)
			if tt.wantErr {
				require.Error(t, err)
				require.EqualError(t, err, tt.errString)
				return
			}
			require.NoError(t, err)
			require.Equal(t, tt.want, got)
		})
	}
}

type MockDo func(*http.Request) (*http.Response, error)

func newMockClient(do MockDo) *mockClient {
	return &mockClient{
		MockFn: do,
	}
}

type mockClient struct {
	MockFn MockDo
}

func (mc *mockClient) Do(r *http.Request) (*http.Response, error) {
	return mc.MockFn(r)
}

func locationsAsBytes() []byte {
	b, err := json.Marshal(locationMocks())
	if err != nil {
		return []byte("")
	}
	return b
}

func locationAsBytes() []byte {
	b, err := json.Marshal(locationMocks()[0])
	if err != nil {
		return []byte("")
	}
	return b
}

func locationMocks() []*OpenStreetMapLocation {
	l1 := &OpenStreetMapLocation{
		DisplayName: "Test street, 1111, Croatia, HR",
		Address: OpenStreetMapAddress{
			City:        "Zagreb",
			Country:     "Croatia",
			CountryCode: "HR",
			Postcode:    "1",
			Road:        "Test street",
			Suburb:      "Test suburb",
		},
		BoundingBox: []string{"1", "2", "3", "4"},
		Latitude:    "1",
		Longitude:   "2",
		Licence:     "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
		Class:       "highway",
		OsmID:       1,
		Importance:  0.5,
		OsmType:     "way",
		PlaceID:     1,
		Type:        "residential",
	}
	l2 := &OpenStreetMapLocation{
		DisplayName: "Test street, 2222, Croatia, HR",
		Address: OpenStreetMapAddress{
			City:        "Split",
			Country:     "Croatia",
			CountryCode: "HR",
			Postcode:    "2",
			Road:        "Test street",
			Suburb:      "Test suburb",
		},
		BoundingBox: []string{"1", "2", "3", "4"},
		Latitude:    "1",
		Longitude:   "2",
		Licence:     "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
		Class:       "highway",
		OsmID:       2,
		Importance:  0.5,
		OsmType:     "way",
		PlaceID:     2,
		Type:        "residential",
	}
	return []*OpenStreetMapLocation{l1, l2}
}
