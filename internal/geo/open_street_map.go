package geo

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"time"

	bikepark_geo_v1 "gitlab.com/thebikepark/bikepark-geo-search/pkg"
)

// {
//     "place_id": 93561778,
//     "licence": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
//     "osm_type": "way",
//     "osm_id": 29622637,
//     "boundingbox": [
//       "45.8050671",
//       "45.8081558",
//       "16.0238608",
//       "16.0264799"
//     ],
//     "lat": "45.8063602",
//     "lon": "16.0249736",
//     "display_name": "Kolarova ulica, Volovčica, Peščenica - Žitnjak, Zagreb, City of Zagreb, 10000 ZAGREB, Croatia",
//     "class": "highway",
//     "type": "residential",
//     "importance": 0.5,
//     "address": {
//       "road": "Kolarova ulica",
//       "neighbourhood": "Volovčica",
//       "suburb": "Peščenica - Žitnjak",
//       "city": "Zagreb",
//       "postcode": "10000 ZAGREB",
//       "country": "Croatia",
//       "country_code": "hr"
//     }
//   }

// OpenStreetMapLocation openstreetmap location data
type OpenStreetMapLocation struct {
	PlaceID     int64                `json:"place_id"`
	Licence     string               `json:"licence"`
	Class       string               `json:"class"`
	Type        string               `json:"type"`
	OsmType     string               `json:"osm_type"`
	OsmID       int64                `json:"osm_id"`
	Latitude    string               `json:"lat"`
	Longitude   string               `json:"lon"`
	Importance  float32              `json:"importance"`
	DisplayName string               `json:"display_name"`
	BoundingBox []string             `json:"boundingbox"`
	Address     OpenStreetMapAddress `json:"address"`
}

// OpenStreetMapAddress openstreetmap address details
type OpenStreetMapAddress struct {
	Road          string `json:"road"`
	Neighbourhood string `json:"neighbourhood"`
	Suburb        string `json:"suburb"`
	City          string `json:"city"`
	Postcode      string `json:"postcode"`
	Country       string `json:"country"`
	CountryCode   string `json:"country_code"`
}

// OpenStreetMap implements Locator interface
// holds info for open street map
type OpenStreetMap struct {
	URL        string
	Format     string
	HTTPClient Client
}

// ToGeoLocation parse open street map location to proto v1
func (osml *OpenStreetMapLocation) ToGeoLocation() (*bikepark_geo_v1.GeoLocation, error) {
	lat, err := strconv.ParseFloat(osml.Latitude, 64)
	if err != nil {
		return nil, err
	}
	lng, err := strconv.ParseFloat(osml.Longitude, 64)
	if err != nil {
		return nil, err
	}

	return &bikepark_geo_v1.GeoLocation{
		Address:     osml.Address.Road,
		City:        osml.Address.City,
		DisplayName: osml.DisplayName,
		Latitude:    lat,
		Longitude:   lng,
	}, nil
}

// Client HTTP client wrapper
type Client interface {
	Do(*http.Request) (resp *http.Response, err error)
}

// NewOpenStreetMap returns new instance of OpenStreetMap locator
func NewOpenStreetMap(URL, format string) *OpenStreetMap {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	http := &http.Client{
		Timeout:   10 * time.Second,
		Transport: tr,
	}

	return &OpenStreetMap{
		URL:        URL,
		Format:     format,
		HTTPClient: http,
	}
}

// Search search locations by address
// https://nominatim.openstreetmap.org/search.php?format=jsonv2&addressdetails=1&q=zagrebački velesajam
func (osm *OpenStreetMap) Search(q string) (Locations, error) {
	params := url.Values{}
	params.Add("format", osm.Format)
	params.Add("addressdetails", "1")
	params.Add("q", q)

	req, err := http.NewRequest(http.MethodGet, osm.URL+"/search.php?"+params.Encode(), nil)
	if err != nil {
		return nil, fmt.Errorf("http.NewRequest(): %w", err)
	}
	resp, err := osm.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("fetch data from OpenStreetMap service, code: %d", resp.StatusCode)
	}

	var osmLs []*OpenStreetMapLocation
	d := json.NewDecoder(resp.Body)
	if err := d.Decode(&osmLs); err != nil {
		return nil, fmt.Errorf("d.Decode(&osmLs) to proper to OpenStreetMap data: %w", err)
	}

	locs := make(Locations, len(osmLs))
	for i, osmL := range osmLs {
		locs[i] = osmL
	}
	return locs, nil
}

// Reverse search location by latitude and longitude
// https://nominatim.openstreetmap.org/reverse.php?format=jsonv2&lat=52.5487429714954&lon=1&addressdetails=1
func (osm *OpenStreetMap) Reverse(lat, lng float64) (Location, error) {
	params := url.Values{}
	params.Add("format", osm.Format)
	params.Add("addressdetails", "1")
	params.Add("lat", fmt.Sprintf("%f", lat))
	params.Add("lon", fmt.Sprintf("%f", lng))

	req, err := http.NewRequest(http.MethodGet, osm.URL+"/reverse.php?"+params.Encode(), nil)
	if err != nil {
		return nil, fmt.Errorf("http.NewRequest(): %w", err)
	}
	resp, err := osm.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("fetch data from OpenStreetMap service, code: %d", resp.StatusCode)
	}

	var osml OpenStreetMapLocation
	if err := json.NewDecoder(resp.Body).Decode(&osml); err != nil {
		// TODO: add fix for error response
		// nominatim can return 200 and { error: "error text" }
		return nil, fmt.Errorf("d.Decode(&osmLs) to proper to OpenStreetMap data: %w", err)
	}
	return &osml, nil
}
