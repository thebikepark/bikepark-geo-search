package geo

import bikepark_geo_v1 "gitlab.com/thebikepark/bikepark-geo-search/pkg"

// Locator implements location serach methods
type Locator interface {
	Search(string) (Locations, error)
	Reverse(float64, float64) (Location, error)
}

// Location imeplements parser to geo location
type Location interface {
	ToGeoLocation() (*bikepark_geo_v1.GeoLocation, error)
}

// Locations list of locations
type Locations []Location

// ToProtos parse locations to bikepark geo location proto
func (locs Locations) ToProtos() ([]*bikepark_geo_v1.GeoLocation, error) {
	geoLocs := make([]*bikepark_geo_v1.GeoLocation, len(locs))
	for i, l := range locs {
		geoL, err := l.ToGeoLocation()
		if err != nil {
			return nil, err
		}
		geoLocs[i] = geoL
	}
	return geoLocs, nil
}
