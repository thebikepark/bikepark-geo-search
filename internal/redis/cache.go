package redis

import (
	"context"
	"encoding/json"
	"time"

	"github.com/go-redis/redis/v8"
	bikepark_geo_v1 "gitlab.com/thebikepark/bikepark-geo-search/pkg"
)

type (
	Config struct {
		Address  string
		Password string
		Timeout  time.Duration
	}

	RedisManager struct {
		redis *redis.Client
	}
)

func NewRedisManager(c Config) *RedisManager {
	rdb := redis.NewClient(&redis.Options{
		Addr:     c.Address,
		Password: c.Password, // no password set
		DB:       0,          // use default DB
	})

	return &RedisManager{
		redis: rdb,
	}
}

func (rm *RedisManager) Store(ctx context.Context, key string, v interface{}) error {
	b, err := json.Marshal(v)
	if err != nil {
		return err
	}
	cmd := rm.redis.Set(ctx, key, string(b), 0)
	if cmd.Err() != nil && cmd.Err() != redis.Nil {
		return cmd.Err()
	}
	return nil
}

func (rm *RedisManager) Get(ctx context.Context, key string) (*bikepark_geo_v1.GeoLocation, error) {
	cmd := rm.redis.Get(ctx, key)
	if cmd.Err() != nil && cmd.Err() != redis.Nil {
		return nil, cmd.Err()
	}

	b, err := cmd.Bytes()
	if err != nil && cmd.Err() != redis.Nil {
		return nil, cmd.Err()
	}

	if len(b) == 0 {
		return nil, nil
	}

	var l *bikepark_geo_v1.GeoLocation
	if err := json.Unmarshal(b, &l); err != nil {
		return nil, err
	}

	return l, nil
}

func (rm *RedisManager) List(ctx context.Context, key string) ([]*bikepark_geo_v1.GeoLocation, error) {
	cmd := rm.redis.Get(ctx, key)
	if cmd.Err() != nil && cmd.Err() != redis.Nil {
		return nil, cmd.Err()
	}

	b, err := cmd.Bytes()
	if err != nil && cmd.Err() != redis.Nil {
		return nil, cmd.Err()
	}

	if len(b) == 0 {
		return nil, nil
	}

	var locations []*bikepark_geo_v1.GeoLocation
	if err := json.Unmarshal(b, &locations); err != nil {
		return nil, err
	}

	return locations, nil
}

func (rm *RedisManager) Close() {
	rm.redis.Close()
}

func (rm *RedisManager) Ping(ctx context.Context) error {
	statusCmd := rm.redis.Ping(ctx)
	if statusCmd.Err() != redis.Nil {
		return statusCmd.Err()
	}
	return nil
}
