// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.20.1
// source: geo.proto

package bikepark_geo_v1

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// BikeParkGeoSearchClient is the client API for BikeParkGeoSearch service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type BikeParkGeoSearchClient interface {
	SearchGeoLocation(ctx context.Context, in *SearchGeoLocationRequest, opts ...grpc.CallOption) (*SearchGeoLocationResponse, error)
	ReverseGeoLocation(ctx context.Context, in *ReverseGeoLocationRequest, opts ...grpc.CallOption) (*GeoLocation, error)
}

type bikeParkGeoSearchClient struct {
	cc grpc.ClientConnInterface
}

func NewBikeParkGeoSearchClient(cc grpc.ClientConnInterface) BikeParkGeoSearchClient {
	return &bikeParkGeoSearchClient{cc}
}

func (c *bikeParkGeoSearchClient) SearchGeoLocation(ctx context.Context, in *SearchGeoLocationRequest, opts ...grpc.CallOption) (*SearchGeoLocationResponse, error) {
	out := new(SearchGeoLocationResponse)
	err := c.cc.Invoke(ctx, "/bikepark.geo.v1.BikeParkGeoSearch/SearchGeoLocation", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *bikeParkGeoSearchClient) ReverseGeoLocation(ctx context.Context, in *ReverseGeoLocationRequest, opts ...grpc.CallOption) (*GeoLocation, error) {
	out := new(GeoLocation)
	err := c.cc.Invoke(ctx, "/bikepark.geo.v1.BikeParkGeoSearch/ReverseGeoLocation", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// BikeParkGeoSearchServer is the server API for BikeParkGeoSearch service.
// All implementations must embed UnimplementedBikeParkGeoSearchServer
// for forward compatibility
type BikeParkGeoSearchServer interface {
	SearchGeoLocation(context.Context, *SearchGeoLocationRequest) (*SearchGeoLocationResponse, error)
	ReverseGeoLocation(context.Context, *ReverseGeoLocationRequest) (*GeoLocation, error)
	mustEmbedUnimplementedBikeParkGeoSearchServer()
}

// UnimplementedBikeParkGeoSearchServer must be embedded to have forward compatible implementations.
type UnimplementedBikeParkGeoSearchServer struct {
}

func (UnimplementedBikeParkGeoSearchServer) SearchGeoLocation(context.Context, *SearchGeoLocationRequest) (*SearchGeoLocationResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SearchGeoLocation not implemented")
}
func (UnimplementedBikeParkGeoSearchServer) ReverseGeoLocation(context.Context, *ReverseGeoLocationRequest) (*GeoLocation, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReverseGeoLocation not implemented")
}
func (UnimplementedBikeParkGeoSearchServer) mustEmbedUnimplementedBikeParkGeoSearchServer() {}

// UnsafeBikeParkGeoSearchServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to BikeParkGeoSearchServer will
// result in compilation errors.
type UnsafeBikeParkGeoSearchServer interface {
	mustEmbedUnimplementedBikeParkGeoSearchServer()
}

func RegisterBikeParkGeoSearchServer(s grpc.ServiceRegistrar, srv BikeParkGeoSearchServer) {
	s.RegisterService(&BikeParkGeoSearch_ServiceDesc, srv)
}

func _BikeParkGeoSearch_SearchGeoLocation_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SearchGeoLocationRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BikeParkGeoSearchServer).SearchGeoLocation(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/bikepark.geo.v1.BikeParkGeoSearch/SearchGeoLocation",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BikeParkGeoSearchServer).SearchGeoLocation(ctx, req.(*SearchGeoLocationRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _BikeParkGeoSearch_ReverseGeoLocation_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReverseGeoLocationRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BikeParkGeoSearchServer).ReverseGeoLocation(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/bikepark.geo.v1.BikeParkGeoSearch/ReverseGeoLocation",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BikeParkGeoSearchServer).ReverseGeoLocation(ctx, req.(*ReverseGeoLocationRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// BikeParkGeoSearch_ServiceDesc is the grpc.ServiceDesc for BikeParkGeoSearch service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var BikeParkGeoSearch_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "bikepark.geo.v1.BikeParkGeoSearch",
	HandlerType: (*BikeParkGeoSearchServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SearchGeoLocation",
			Handler:    _BikeParkGeoSearch_SearchGeoLocation_Handler,
		},
		{
			MethodName: "ReverseGeoLocation",
			Handler:    _BikeParkGeoSearch_ReverseGeoLocation_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "geo.proto",
}
