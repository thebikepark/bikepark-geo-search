// Code generated by protoc-gen-grpc-gateway. DO NOT EDIT.
// source: geo.proto

/*
Package bikepark_geo_v1 is a reverse proxy.

It translates gRPC into RESTful JSON APIs.
*/
package bikepark_geo_v1

import (
	"context"
	"io"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/grpc-ecosystem/grpc-gateway/v2/utilities"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"
)

// Suppress "imported and not used" errors
var _ codes.Code
var _ io.Reader
var _ status.Status
var _ = runtime.String
var _ = utilities.NewDoubleArray
var _ = metadata.Join

var (
	filter_BikeParkGeoSearch_SearchGeoLocation_0 = &utilities.DoubleArray{Encoding: map[string]int{}, Base: []int(nil), Check: []int(nil)}
)

func request_BikeParkGeoSearch_SearchGeoLocation_0(ctx context.Context, marshaler runtime.Marshaler, client BikeParkGeoSearchClient, req *http.Request, pathParams map[string]string) (proto.Message, runtime.ServerMetadata, error) {
	var protoReq SearchGeoLocationRequest
	var metadata runtime.ServerMetadata

	if err := req.ParseForm(); err != nil {
		return nil, metadata, status.Errorf(codes.InvalidArgument, "%v", err)
	}
	if err := runtime.PopulateQueryParameters(&protoReq, req.Form, filter_BikeParkGeoSearch_SearchGeoLocation_0); err != nil {
		return nil, metadata, status.Errorf(codes.InvalidArgument, "%v", err)
	}

	msg, err := client.SearchGeoLocation(ctx, &protoReq, grpc.Header(&metadata.HeaderMD), grpc.Trailer(&metadata.TrailerMD))
	return msg, metadata, err

}

func local_request_BikeParkGeoSearch_SearchGeoLocation_0(ctx context.Context, marshaler runtime.Marshaler, server BikeParkGeoSearchServer, req *http.Request, pathParams map[string]string) (proto.Message, runtime.ServerMetadata, error) {
	var protoReq SearchGeoLocationRequest
	var metadata runtime.ServerMetadata

	if err := req.ParseForm(); err != nil {
		return nil, metadata, status.Errorf(codes.InvalidArgument, "%v", err)
	}
	if err := runtime.PopulateQueryParameters(&protoReq, req.Form, filter_BikeParkGeoSearch_SearchGeoLocation_0); err != nil {
		return nil, metadata, status.Errorf(codes.InvalidArgument, "%v", err)
	}

	msg, err := server.SearchGeoLocation(ctx, &protoReq)
	return msg, metadata, err

}

var (
	filter_BikeParkGeoSearch_ReverseGeoLocation_0 = &utilities.DoubleArray{Encoding: map[string]int{}, Base: []int(nil), Check: []int(nil)}
)

func request_BikeParkGeoSearch_ReverseGeoLocation_0(ctx context.Context, marshaler runtime.Marshaler, client BikeParkGeoSearchClient, req *http.Request, pathParams map[string]string) (proto.Message, runtime.ServerMetadata, error) {
	var protoReq ReverseGeoLocationRequest
	var metadata runtime.ServerMetadata

	if err := req.ParseForm(); err != nil {
		return nil, metadata, status.Errorf(codes.InvalidArgument, "%v", err)
	}
	if err := runtime.PopulateQueryParameters(&protoReq, req.Form, filter_BikeParkGeoSearch_ReverseGeoLocation_0); err != nil {
		return nil, metadata, status.Errorf(codes.InvalidArgument, "%v", err)
	}

	msg, err := client.ReverseGeoLocation(ctx, &protoReq, grpc.Header(&metadata.HeaderMD), grpc.Trailer(&metadata.TrailerMD))
	return msg, metadata, err

}

func local_request_BikeParkGeoSearch_ReverseGeoLocation_0(ctx context.Context, marshaler runtime.Marshaler, server BikeParkGeoSearchServer, req *http.Request, pathParams map[string]string) (proto.Message, runtime.ServerMetadata, error) {
	var protoReq ReverseGeoLocationRequest
	var metadata runtime.ServerMetadata

	if err := req.ParseForm(); err != nil {
		return nil, metadata, status.Errorf(codes.InvalidArgument, "%v", err)
	}
	if err := runtime.PopulateQueryParameters(&protoReq, req.Form, filter_BikeParkGeoSearch_ReverseGeoLocation_0); err != nil {
		return nil, metadata, status.Errorf(codes.InvalidArgument, "%v", err)
	}

	msg, err := server.ReverseGeoLocation(ctx, &protoReq)
	return msg, metadata, err

}

// RegisterBikeParkGeoSearchHandlerServer registers the http handlers for service BikeParkGeoSearch to "mux".
// UnaryRPC     :call BikeParkGeoSearchServer directly.
// StreamingRPC :currently unsupported pending https://github.com/grpc/grpc-go/issues/906.
// Note that using this registration option will cause many gRPC library features to stop working. Consider using RegisterBikeParkGeoSearchHandlerFromEndpoint instead.
func RegisterBikeParkGeoSearchHandlerServer(ctx context.Context, mux *runtime.ServeMux, server BikeParkGeoSearchServer) error {

	mux.Handle("GET", pattern_BikeParkGeoSearch_SearchGeoLocation_0, func(w http.ResponseWriter, req *http.Request, pathParams map[string]string) {
		ctx, cancel := context.WithCancel(req.Context())
		defer cancel()
		var stream runtime.ServerTransportStream
		ctx = grpc.NewContextWithServerTransportStream(ctx, &stream)
		inboundMarshaler, outboundMarshaler := runtime.MarshalerForRequest(mux, req)
		var err error
		ctx, err = runtime.AnnotateIncomingContext(ctx, mux, req, "/bikepark.geo.v1.BikeParkGeoSearch/SearchGeoLocation", runtime.WithHTTPPathPattern("/api/v1/geolocations"))
		if err != nil {
			runtime.HTTPError(ctx, mux, outboundMarshaler, w, req, err)
			return
		}
		resp, md, err := local_request_BikeParkGeoSearch_SearchGeoLocation_0(ctx, inboundMarshaler, server, req, pathParams)
		md.HeaderMD, md.TrailerMD = metadata.Join(md.HeaderMD, stream.Header()), metadata.Join(md.TrailerMD, stream.Trailer())
		ctx = runtime.NewServerMetadataContext(ctx, md)
		if err != nil {
			runtime.HTTPError(ctx, mux, outboundMarshaler, w, req, err)
			return
		}

		forward_BikeParkGeoSearch_SearchGeoLocation_0(ctx, mux, outboundMarshaler, w, req, resp, mux.GetForwardResponseOptions()...)

	})

	mux.Handle("GET", pattern_BikeParkGeoSearch_ReverseGeoLocation_0, func(w http.ResponseWriter, req *http.Request, pathParams map[string]string) {
		ctx, cancel := context.WithCancel(req.Context())
		defer cancel()
		var stream runtime.ServerTransportStream
		ctx = grpc.NewContextWithServerTransportStream(ctx, &stream)
		inboundMarshaler, outboundMarshaler := runtime.MarshalerForRequest(mux, req)
		var err error
		ctx, err = runtime.AnnotateIncomingContext(ctx, mux, req, "/bikepark.geo.v1.BikeParkGeoSearch/ReverseGeoLocation", runtime.WithHTTPPathPattern("/api/v1/geolocations:reverse"))
		if err != nil {
			runtime.HTTPError(ctx, mux, outboundMarshaler, w, req, err)
			return
		}
		resp, md, err := local_request_BikeParkGeoSearch_ReverseGeoLocation_0(ctx, inboundMarshaler, server, req, pathParams)
		md.HeaderMD, md.TrailerMD = metadata.Join(md.HeaderMD, stream.Header()), metadata.Join(md.TrailerMD, stream.Trailer())
		ctx = runtime.NewServerMetadataContext(ctx, md)
		if err != nil {
			runtime.HTTPError(ctx, mux, outboundMarshaler, w, req, err)
			return
		}

		forward_BikeParkGeoSearch_ReverseGeoLocation_0(ctx, mux, outboundMarshaler, w, req, resp, mux.GetForwardResponseOptions()...)

	})

	return nil
}

// RegisterBikeParkGeoSearchHandlerFromEndpoint is same as RegisterBikeParkGeoSearchHandler but
// automatically dials to "endpoint" and closes the connection when "ctx" gets done.
func RegisterBikeParkGeoSearchHandlerFromEndpoint(ctx context.Context, mux *runtime.ServeMux, endpoint string, opts []grpc.DialOption) (err error) {
	conn, err := grpc.Dial(endpoint, opts...)
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			if cerr := conn.Close(); cerr != nil {
				grpclog.Infof("Failed to close conn to %s: %v", endpoint, cerr)
			}
			return
		}
		go func() {
			<-ctx.Done()
			if cerr := conn.Close(); cerr != nil {
				grpclog.Infof("Failed to close conn to %s: %v", endpoint, cerr)
			}
		}()
	}()

	return RegisterBikeParkGeoSearchHandler(ctx, mux, conn)
}

// RegisterBikeParkGeoSearchHandler registers the http handlers for service BikeParkGeoSearch to "mux".
// The handlers forward requests to the grpc endpoint over "conn".
func RegisterBikeParkGeoSearchHandler(ctx context.Context, mux *runtime.ServeMux, conn *grpc.ClientConn) error {
	return RegisterBikeParkGeoSearchHandlerClient(ctx, mux, NewBikeParkGeoSearchClient(conn))
}

// RegisterBikeParkGeoSearchHandlerClient registers the http handlers for service BikeParkGeoSearch
// to "mux". The handlers forward requests to the grpc endpoint over the given implementation of "BikeParkGeoSearchClient".
// Note: the gRPC framework executes interceptors within the gRPC handler. If the passed in "BikeParkGeoSearchClient"
// doesn't go through the normal gRPC flow (creating a gRPC client etc.) then it will be up to the passed in
// "BikeParkGeoSearchClient" to call the correct interceptors.
func RegisterBikeParkGeoSearchHandlerClient(ctx context.Context, mux *runtime.ServeMux, client BikeParkGeoSearchClient) error {

	mux.Handle("GET", pattern_BikeParkGeoSearch_SearchGeoLocation_0, func(w http.ResponseWriter, req *http.Request, pathParams map[string]string) {
		ctx, cancel := context.WithCancel(req.Context())
		defer cancel()
		inboundMarshaler, outboundMarshaler := runtime.MarshalerForRequest(mux, req)
		var err error
		ctx, err = runtime.AnnotateContext(ctx, mux, req, "/bikepark.geo.v1.BikeParkGeoSearch/SearchGeoLocation", runtime.WithHTTPPathPattern("/api/v1/geolocations"))
		if err != nil {
			runtime.HTTPError(ctx, mux, outboundMarshaler, w, req, err)
			return
		}
		resp, md, err := request_BikeParkGeoSearch_SearchGeoLocation_0(ctx, inboundMarshaler, client, req, pathParams)
		ctx = runtime.NewServerMetadataContext(ctx, md)
		if err != nil {
			runtime.HTTPError(ctx, mux, outboundMarshaler, w, req, err)
			return
		}

		forward_BikeParkGeoSearch_SearchGeoLocation_0(ctx, mux, outboundMarshaler, w, req, resp, mux.GetForwardResponseOptions()...)

	})

	mux.Handle("GET", pattern_BikeParkGeoSearch_ReverseGeoLocation_0, func(w http.ResponseWriter, req *http.Request, pathParams map[string]string) {
		ctx, cancel := context.WithCancel(req.Context())
		defer cancel()
		inboundMarshaler, outboundMarshaler := runtime.MarshalerForRequest(mux, req)
		var err error
		ctx, err = runtime.AnnotateContext(ctx, mux, req, "/bikepark.geo.v1.BikeParkGeoSearch/ReverseGeoLocation", runtime.WithHTTPPathPattern("/api/v1/geolocations:reverse"))
		if err != nil {
			runtime.HTTPError(ctx, mux, outboundMarshaler, w, req, err)
			return
		}
		resp, md, err := request_BikeParkGeoSearch_ReverseGeoLocation_0(ctx, inboundMarshaler, client, req, pathParams)
		ctx = runtime.NewServerMetadataContext(ctx, md)
		if err != nil {
			runtime.HTTPError(ctx, mux, outboundMarshaler, w, req, err)
			return
		}

		forward_BikeParkGeoSearch_ReverseGeoLocation_0(ctx, mux, outboundMarshaler, w, req, resp, mux.GetForwardResponseOptions()...)

	})

	return nil
}

var (
	pattern_BikeParkGeoSearch_SearchGeoLocation_0 = runtime.MustPattern(runtime.NewPattern(1, []int{2, 0, 2, 1, 2, 2}, []string{"api", "v1", "geolocations"}, ""))

	pattern_BikeParkGeoSearch_ReverseGeoLocation_0 = runtime.MustPattern(runtime.NewPattern(1, []int{2, 0, 2, 1, 2, 2}, []string{"api", "v1", "geolocations"}, "reverse"))
)

var (
	forward_BikeParkGeoSearch_SearchGeoLocation_0 = runtime.ForwardResponseMessage

	forward_BikeParkGeoSearch_ReverseGeoLocation_0 = runtime.ForwardResponseMessage
)
